" Vim color file
"
" Author: Tomas Restrepo <tomas@winterdom.com>
" Author: Pierre Bourdon <delroth@gmail.com> (modifications)
" Author: Baptiste Marchand <baptistemarchand42@gmail.com> (modifications)
"
" Note: Based on the monokai theme for textmate
" by Wimer Hazenberg and its darker variant
" by Hamish Stuart Macpherson
"

hi clear

set background=dark
if version > 580
    " no guarantees for version 5.8 and below, but this makes it stop
    " complaining
    hi clear
    if exists("syntax_on")
        syntax reset
    endif
endif
let g:colors_name="bap"

if &t_Co > 255

   "------ Environment ------
   "                               background color
   hi Normal          ctermfg=231  ctermbg=233
   " column with line number
   hi LineNr          ctermfg=232 ctermbg=234
   "  status line (with file's name, mode, etc)
   "  /!\ fg and bg are inverted
   hi StatusLine      ctermfg=235 ctermbg=232
   "  status line when buffer doesn't have focus
   hi StatusLineNC    ctermfg=235 ctermbg=233
   "  separator when vertical split
   hi VertSplit       ctermfg=235 ctermbg=235 cterm=bold
   "  Error message
   hi ErrorMsg        ctermfg=88 ctermbg=233    cterm=bold

   "------ Syntax ------

   "  int i = 10;
   hi Boolean         ctermfg=135
   hi Number          ctermfg=135
   hi Float           ctermfg=135
   "  char c = 'X'
   hi Character       ctermfg=227
   "  char* s = \"coucou"
   hi String          ctermfg=227
   "  printf("%d")
   hi SpecialChar     ctermfg=227               cterm=bold
   "  if
   hi Conditional     ctermfg=130               cterm=bold
   "  while for
   hi Repeat          ctermfg=208               cterm=bold
   "  #define
   hi Macro           ctermfg=94
   "  #include
   hi PreProc         ctermfg=95
   "  struct
   hi Structure       ctermfg=70
   "  int float
   hi Type            ctermfg=178               cterm=none
   "  NULL
   hi Constant        ctermfg=214              cterm=bold
   "  // this is a comment
   hi Comment         ctermfg=242
   "  try catch throw
   hi Exception       ctermfg=123               cterm=bold
   "  static volatile
   hi StorageClass    ctermfg=208
   "  other parent when one is selected
   hi MatchParen      ctermfg=124 ctermbg=16    cterm=bold
   "  my_label:
   hi Label           ctermfg=111               cterm=bold
   "  return
   hi Statement       ctermfg=208               cterm=bold
   "  ~~~ at the end of a file
   hi NonText         ctermfg=232               cterm=bold
   "  Makefile variables
   hi Identifier      ctermfg=220
   "  Makefile rules
   hi Function        ctermfg=45
   "  operators in vim scripts
   hi Operator        ctermfg=153



   hi Cursor          ctermfg=16  ctermbg=253
   hi Debug           ctermfg=225               cterm=bold
   hi Define          ctermfg=81
   hi Delimiter       ctermfg=241

   hi DiffAdd                     ctermbg=24
   hi DiffChange      ctermfg=181 ctermbg=239
   hi DiffDelete      ctermfg=162 ctermbg=53
   hi DiffText                    ctermbg=102 cterm=bold

   hi Directory       ctermfg=118               cterm=bold
   hi Error           ctermfg=219 ctermbg=89
   hi FoldColumn      ctermfg=67  ctermbg=16
   hi Folded          ctermfg=67  ctermbg=16
   hi Ignore          ctermfg=244 ctermbg=232
   hi IncSearch       ctermfg=193 ctermbg=16

   hi Keyword         ctermfg=191               cterm=bold
   hi SpecialKey      ctermfg=81

   hi ModeMsg         ctermfg=229
   hi MoreMsg         ctermfg=229

   " complete menu
   hi Pmenu           ctermfg=81  ctermbg=16
   hi PmenuSel                    ctermbg=244
   hi PmenuSbar                   ctermbg=232
   hi PmenuThumb      ctermfg=81

   hi PreCondit       ctermfg=118               cterm=bold
   hi Question        ctermfg=81
   hi Search          ctermfg=253 ctermbg=66

   " marks column
   hi SignColumn      ctermfg=118 ctermbg=235
   hi SpecialComment  ctermfg=245               cterm=bold
   hi Special         ctermfg=81  ctermbg=232
   hi SpecialKey      ctermfg=167               cterm=bold

   hi Tag             ctermfg=161
   hi Title           ctermfg=166
   hi Todo            ctermfg=231 ctermbg=232   cterm=bold

   hi Typedef         ctermfg=81
   hi Underlined      ctermfg=244               cterm=underline

   hi VisualNOS                   ctermbg=238
   hi Visual                      ctermbg=235
   hi WarningMsg      ctermfg=231 ctermbg=238   cterm=bold
   hi WildMenu        ctermfg=81  ctermbg=16

   hi CursorLine                  ctermbg=234   cterm=none
   hi CursorColumn                ctermbg=234

   hi ColorColumn     ctermfg=252 ctermbg=232
end
