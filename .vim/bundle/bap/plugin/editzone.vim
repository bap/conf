" editzone.vim
" Parameters related to the edition zone

if version < 703
  set number
else
  set relativenumber
endif

if version >= 703
  set colorcolumn=+1
endif

"set ruler
set laststatus=2
set statusline=%f\ %l\|%c\ %m%=%p%%\ (%Y%R)

set wildmenu
set hid
set noerrorbells
set novisualbell

set mouse=a
