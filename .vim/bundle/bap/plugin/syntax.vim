" syntax.vim
" Sets syntax highlighting parameters if available.

if has('syntax')
    syntax on
    set background=dark

    autocmd ColorScheme * highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen

    " Trailing whitespaces
    highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
    match ExtraWhitespace /\s\+$/

    set t_Co=256
    colorscheme bap

    " Python specific options
    let python_highlight_builtins = 1
    let python_highlight_exceptions = 1
    let python_highlight_space_errors = 1
endif
