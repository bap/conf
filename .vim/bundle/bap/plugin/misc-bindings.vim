" misc-bindings.vim
" Every miscellaneous key binding will go here.

" « That stupid goddamned help key that you will invaribly hit constantly
" while aiming for escape » -- Steve Losh
inoremap <F1> <esc>
vnoremap <F1> <esc>
nnoremap <F1> <esc>

" « it's one less key to hit every time I want to save a file »
"   -- Steve Losh (again)
nnoremap ; :
vnoremap ; :

map <S-LEFT> <ESC>:bp<CR>
map <S-RIGHT> <ESC>:bn<CR>
map <SPACE> <C-w>

map tn <ESC>:tabnext<CR>
map tp <ESC>:tabprevious<CR>

map <C-PageUp> <ESC>:tabprevious<CR>
map <C-PageDown> <ESC>:tabnext<CR>

nmap <C-t> <ESC>:tabnew<CR>
imap <C-t> <ESC>:tabnew<CR>

nmap <C-f> :CommandT<CR>

map <F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>

nmap <S-c> \c<space><CR>
vmap <S-c> \c<space><CR>
