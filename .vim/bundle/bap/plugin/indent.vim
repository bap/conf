" indent.vim
" Defines indentation behavior

set tabstop=8
set softtabstop=8
set shiftwidth=2
set expandtab
set smarttab

set autoindent smartindent
