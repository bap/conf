#!/bin/sh

export PATH=$PATH:/usr/local/bin
# export TERM=xterm-256color
# export NNTPSERVER="news.epita.fr"
export EMAIL="baptistemarchand42@gmail.com"
export NAME='Baptiste'
export FULLNAME='Baptiste Marchand'
export REPLYTO=${EMAIL}
export LANG="en_US.UTF-8"
# export TERMINAL=urxvt

[ -x "`which less`" ] && export PAGER=less
[ -x "`which most`" ] && export PAGER=most

# WMII_IS_RUNNING=`ps aux | grep wmii | awk '/[^"grep"] wmii$/'`
# if [ -n "$WMII_IS_RUNNING" ]; then
#   PROMPT_COMMAND='echo -n "$(dirs)" | wmiir write /client/sel/label'
# fi
