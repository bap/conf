#!/bin/sh

alias emacs='emacs -nw'
alias e='emacs -nw'
alias i='sudo apt-get install'
alias s='sudo apt-cache search'
alias la='ls -a -G'
alias sl='ls'
alias du='du -h'
alias df='df -h'
alias mv='mv -v'
alias cp='cp -v'
alias alsamixer='gnome-alsamixer'
alias pacman='sudo pacman-color'
alias qemu='qemu-system-i386'
alias z='i3lock -i ~/.wallpaper.png'
alias grep="grep --color"
alias dc="cd"
alias gf="fg"
alias network="trayer --widthtype request & nm-applet && killall trayer"
alias r="ssh liu mount --remount,rw /opt"
alias ec='emacsclient -n'
alias st='svn st -q'

#git
alias gl='git log --oneline --graph --decorate --all'
alias g='git'
alias gst='git status -sb'
alias gd='git diff'
alias gup='git pull --rebase'
alias gp='git push'
alias gd='git diff'
alias gc='git commit -v'
alias gco='git checkout'
alias gcp='git cherry-pick'
alias ga='git add'
alias gm='git merge'
alias grh='git reset HEAD'
alias grhh='git reset HEAD --hard'
alias gk='gitk --all --branches'
alias gsts='git stash show --text'
alias gsta='git stash'
alias gstp='git stash pop'
# Will cd into the top of the current repository
# or submodule.
alias grt='cd $(git rev-parse --show-toplevel || echo ".")'
