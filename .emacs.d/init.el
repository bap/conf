;; Emacs24 configuration
;; Baptiste Marchand 2014

(add-to-list 'load-path "~/.emacs.d/scripts/")

(load "package_management.el") ;; Auto install packages if needed
(load "interface.el")
(load "bindings.el") ;; Generic bindings
(load "bepo.el")     ;; Bépo bindings
(load "code.el")

;; Mac OSX
(setq mac-option-modifier 'super)
(setq mac-command-modifier 'meta)

;; Backup files
(setq backup-directory-alist `(("." . "~/.emacs.d/saves")))
(setq delete-old-versions t
  kept-new-versions 6
  kept-old-versions 2
  version-control t)

;; Make text mode the default for new buffers
(setq default-major-mode 'text-mode)

;; pending delete
(pending-delete-mode t)

;;TRAMP
(require 'tramp)
(setq tramp-default-method "ssh")

;; Enable some commands
(put 'narrow-to-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" default))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
