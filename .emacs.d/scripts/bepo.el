(keyboard-translate ?\C-y ?\C-x)
(setq local-function-key-map (delq '(kp-tab . [9]) local-function-key-map))
(keyboard-translate ?\C-i ?\C-x)

;; moving
(global-set-key (kbd "C-n") 'forward-char)
(global-set-key (kbd "C-t") 'backward-char)
(global-set-key (kbd "C-s") 'next-line)
(global-set-key (kbd "C-r") 'previous-line)

(global-set-key (kbd "M-n") 'forward-word)
(global-set-key (kbd "M-t") 'backward-word)
(global-set-key (kbd "M-s") 'scroll-up-command)
(global-set-key (kbd "M-r") 'scroll-down-command)
(global-set-key (kbd "C-M-s") 'scroll-other-window)

(global-set-key (kbd "C-x n") 'next-buffer)
(global-set-key (kbd "C-x t") 'previous-buffer)

(global-set-key (kbd "C-M-t") 'windmove-left)
(global-set-key (kbd "C-M-n") 'windmove-right)
(global-set-key (kbd "C-M-r") 'windmove-up)
(global-set-key (kbd "C-M-s") 'windmove-down)


;; deleting
(global-set-key (kbd "C-v") 'backward-delete-char-untabify)
(global-set-key (kbd "M-v") 'backward-kill-word)
(global-set-key (kbd "C-j") 'delete-char)
(global-set-key (kbd "M-j") 'kill-word)

(add-hook 'c-mode-hook
          (lambda ()
            (define-key c-mode-map (kbd "C-d") 'kill-line)
            (define-key c++-mode-map (kbd "M-j") 'kill-word)
            ))

(add-hook 'c++-mode-hook
          (lambda ()
            (define-key c++-mode-map (kbd "C-d") 'kill-line)
            (define-key c++-mode-map (kbd "M-j") 'kill-word)
            ))

(global-set-key (kbd "C-d") 'kill-line)
(global-set-key (kbd "M-d") 'kill-region)
(global-set-key (kbd "C-l") 'yank)
(global-set-key (kbd "M-l") 'yank)
(global-set-key (kbd "s-(") (lambda() (interactive) (insert "[")))
(global-set-key (kbd "s-)") (lambda() (interactive) (insert "]")))
(global-set-key (kbd "s-b") (lambda() (interactive) (insert "|")))
(global-set-key (kbd "s-à") (lambda() (interactive) (insert "\\")))

;; searching
(global-set-key (kbd "C-q") 'isearch-forward)
(define-key isearch-mode-map "\C-q" 'isearch-repeat-forward)
(global-set-key (kbd "M-q") 'isearch-backward)
(define-key isearch-mode-map "\M-q" 'isearch-repeat-backward)

;; kill buffer
(global-set-key (kbd "C-b") 'ido-kill-buffer)

;; comment region
(global-set-key (kbd "C-é") 'comment-or-uncomment-region)

;; easy undo
(global-set-key (kbd "ç") 'undo)

;; expand region
(global-set-key (kbd "C-k") 'er/expand-region)
