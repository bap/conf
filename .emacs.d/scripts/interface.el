;; remove menu
(menu-bar-mode -1)
(toggle-scroll-bar -1)
(tool-bar-mode -1)

;; Remove startup message
(setq inhibit-startup-echo-area-message t)
(setq inhibit-startup-message t)

;; Set font and theme
(load-theme 'solarized-dark t)
(set-frame-font "Inconsolata-13" nil t)

;; Display column number
(column-number-mode t)

;; Highlight current line
;;(global-hl-line-mode 1)

;; Remove the 'yes or no' prompts
(fset 'yes-or-no-p 'y-or-n-p)

;; Enable ido-mode
(ido-mode t)
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)

;; Do not search files in other directories
;;(setq ido-auto-merge-work-directories-length -1)

;; Shortcuts to move between windows
(global-set-key [(meta left)] 'windmove-left)
(global-set-key [(meta right)] 'windmove-right)
(global-set-key [(meta up)] 'windmove-up)
(global-set-key [(meta down)] 'windmove-down)

;; Shortcuts to resize windows
(global-set-key [(shift left)] 'shrink-window-horizontally)
(global-set-key [(shift right)] 'enlarge-window-horizontally)
(global-set-key [(shift up)] 'shrink-window)
(global-set-key [(shift down)] 'enlarge-window)

;; speedbar
(global-set-key (kbd "<f9>") 'ecb-activate)

;; skip annying buffers
;;------------------------------------------------------------------------------
(setq skippable-buffers '("*Messages*" "*scratch*" "*Help*"
                          "*compilation*" "*Disabled Command*" "*Irony*"))
(defun my-next-buffer ()
  "next-buffer that skips certain buffers"
  (interactive)
  (next-buffer)
  (while (member (buffer-name) skippable-buffers)
    (next-buffer)))

(defun my-previous-buffer ()
  "previous-buffer that skips certain buffers"
  (interactive)
  (previous-buffer)
  (while (member (buffer-name) skippable-buffers)
    (previous-buffer)))

(global-set-key [remap next-buffer] 'my-next-buffer)
(global-set-key [remap previous-buffer] 'my-previous-buffer)
;;------------------------------------------------------------------------------

(setq ecb-tip-of-the-day nil)

(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

(setq x-select-enable-clipboard t
      x-select-enable-primary t
      save-interprogram-paste-before-kill t
      apropos-do-all t
      mouse-yank-at-point t)

(defun toggle-frame-split ()
  "If the frame is split vertically, split it horizontally or vice versa.
Assumes that the frame is only split into two."
  (interactive)
  (unless (= (length (window-list)) 2) (error "Can only toggle a frame split in two"))
  (let ((split-vertically-p (window-combined-p)))
    (delete-window) ; closes current window
    (if split-vertically-p
        (split-window-horizontally)
      (split-window-vertically)) ; gives us a split with the other window twice
    (switch-to-buffer nil))) ; restore the original window in this part of the frame

;; I don't use the default binding of 'C-x 5', so use toggle-frame-split instead
(global-set-key (kbd "<f2>") 'toggle-frame-split)

