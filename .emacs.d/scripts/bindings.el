;; Compiling
(global-set-key (kbd "C-c C-c") 'compile)
(global-set-key (kbd "C-c c") 'recompile)

;; Go to next error
(global-set-key (kbd "C-c e") 'next-error)

;; shell command
(global-set-key (kbd "<f1>") 'shell-command)

;; ido menu
(global-set-key (kbd "C-f") 'idomenu)

;; ibuffer
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; visal regexp
(global-set-key (kbd "M-%") 'vr/query-replace)

;; Don't exit my emacs!
(global-set-key (kbd "C-x C-c") 'emacs-uptime)
(global-set-key (kbd "<f12>") 'save-buffers-kill-emacs)
