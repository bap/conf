;;* General
;;------------------------------------------------------------------------------
(setq standard-indent 4) ;; Indentation
(setq-default indent-tabs-mode nil) ;; Use spaces instead of tabs
(setq-default show-trailing-whitespace t) ;; Show trailing whitespace
(show-paren-mode t) ;; Show matching parentheses
;;------------------------------------------------------------------------------


;;* C/C++
;;------------------------------------------------------------------------------
(setq c-default-style "K&R" c-basic-offset 4) ;; Indentation mode
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode)) ;; C++ mode for .h files
(c-set-offset 'access-label -2) ;; C++ labels (public, private, ...)

;; Completion with Irony
;;----------------------
;; The Clang installation missed the system lib directory
;; (setq irony-libclang-additional-flags
;;       '("-I" "/usr/lib/llvm-3.3/lib"))

;; (require 'auto-complete)
;; (require 'yasnippet)
;; (require 'irony) ;; Note: hit `C-c C-b' to open build menu

;; ;; the ac plugin will be activated in each buffer using irony-mode
;; (irony-enable 'ac)

;; (defun my-c++-hooks ()
;;   "Enable the hooks in the preferred order: 'yas -> auto-complete -> irony'."
;;   ;; be cautious, if yas is not enabled before (auto-complete-mode 1), overlays
;;   ;; *may* persist after an expansion.
;;   (yas/minor-mode-on)
;;   (auto-complete-mode 1)

;;   ;; avoid enabling irony-mode in modes that inherits c-mode, e.g: php-mode
;;   (when (member major-mode irony-known-modes)
;;     (irony-mode 1)))

;; (add-hook 'c++-mode-hook 'my-c++-hooks)
;; (add-hook 'c-mode-hook 'my-c++-hooks)
;;------------------------------------------------------------------------------


;;* GLSL
;;------------------------------------------------------------------------------
(autoload 'glsl-mode "glsl-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.vert\\'" . glsl-mode))
(add-to-list 'auto-mode-alist '("\\.frag\\'" . glsl-mode))
(add-to-list 'auto-mode-alist '("\\.shader\\'" . glsl-mode))
;;------------------------------------------------------------------------------


(add-to-list 'auto-mode-alist '("\\.scss\\'" . sass-mode))
(add-to-list 'auto-mode-alist '("\\.sass\\'" . sass-mode))

;;* Python
;;------------------------------------------------------------------------------
(add-hook 'python-mode-hook
      (lambda ()
        (setq tab-width 4)
        (setq python-indent 4)))

(add-hook 'python-mode-hook 'flymake-mode)

(when (load "flymake" t)
  (defun flymake-pyflakes-init ()
    (let* ((temp-file (flymake-init-create-temp-buffer-copy
               'flymake-create-temp-inplace))
       (local-file (file-relative-name
            temp-file
            (file-name-directory buffer-file-name))))
      (list "pep8_120"  (list local-file))))
   (add-to-list 'flymake-allowed-file-name-masks
             '("\\.py\\'" flymake-pyflakes-init)))
;;------------------------------------------------------------------------------


(autoload 'hack-mode "hack-mode" nil t)
