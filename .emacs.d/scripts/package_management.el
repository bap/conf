;;package archive
(require 'package)
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")
                         ("tromey" . "http://tromey.com/elpa/")))

;; el-get
;;------------------------------------------------------------------------------
(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(add-to-list 'el-get-recipe-path "~/.emacs.d/el-get-user/recipes")
(add-to-list 'el-get-user-package-directory "~/.emacs.d/el-get/el-get-init-files/")
(el-get 'sync)

(setq my-packages
      '(auto-complete cl-lib cmake-mode el-get expand-region fuzzy git-emacs git-modes
        idomenu json markdown-mode popup popwin psvn qml-mode smart-tab
        sublime-themes yasnippet protobuf-mode))

(el-get 'sync my-packages)
;;------------------------------------------------------------------------------
