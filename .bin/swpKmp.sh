#!/bin/bash

if [ "$(setxkbmap -query | grep 'bepo')" ]
then
    setxkbmap us
    xmodmap ~/.xmodmap
    xset -led named "Scroll Lock"
else
    setxkbmap fr bepo
    xmodmap ~/.xmodmap
    xset led named "Scroll Lock"
fi
