#!/bin/sh
PWD=$(pwd)
CONF_FILES=".emacs.d .xinitrc .bashrc .hgrc .gitconfig .shell-config .Xresources .wmii .zshrc .zshrc.pre .vimrc .vim .bin .weechat .slrnrc .signature .screenrc .mpd .i3 .i3status.conf"

for i in $CONF_FILES
do
  echo -e "\n*** installing $i"
  ln -s -v -i ${PWD}/$i ~
done

echo -e "\n*** installing .ssh"
mkdir -p ~/.ssh
ln -s -v -i $PWD/.ssh/config ~/.ssh
