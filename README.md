Configuration Files
===================

These are all my configuration files.
I try to keep them light and with the least dependencies possible.
Feel free to copy and modify them.

If you find an error in my configuration please send me an email at
<baptistemarchand42@gmail.com>

Install
=======

The install script creates simlinks in your home directory so you can
stay up to date. The script will warn you before erasing any configuration
you might already have.

Misc
====

weechat
-------
You will need weechat >= 0.3.7.
If you have troubles with accents you might need to
install libncursesw5-dev and then resinstall weechat from source.

mpd
---
This conf works with mpd running as a normal user

        mkdir -p ~/.mpd/playlists
        touch ~/.mpd/{mpd.db,mpd.log,mpd.pid,mpdstate}

emacs
-----
You will need emacs24 and a terminal with
256 colors like rxvt-unicode-256color

vim
---
You will need a terminal with
256 colors like rxvt-unicode-256color
