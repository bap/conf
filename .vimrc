" .vimrc
" Called before .vim/plugin to init Pathogen

call pathogen#runtime_append_all_bundles()
call pathogen#helptags()
